# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Diciembre 2020
# Clase Cono
# Descripción: Una clase que mantiene todos los componentes necesarios para la creación del cono y sus diferentes transformaciones
# Es llamada por la clase main.

from OpenGL.GL import *
from OpenGL.GLU import *

import pygame

class Textura:
    #inicializador
    def __init__(self,txt):

        # Load the textures
        if txt == 1:
            self.texture_surface = pygame.image.load("tiburonballena.jpg")
        if txt == 2:
            self.texture_surface = pygame.image.load("sushitex.png")
        if txt == 3:
            self.texture_surface = pygame.image.load("ladrillos.jpg")
        if txt == 4:
            self.texture_surface = pygame.image.load("metal.jpg")

        glEnable(GL_TEXTURE_2D)

        # Retrieve the texture data
        self.texture_data = pygame.image.tostring(self.texture_surface, 'RGB', True)
        # Generate a texture id
        self.texture_id = glGenTextures(1)
        # Tell OpenGL we will be using this texture id for texture operations
        glBindTexture(GL_TEXTURE_2D, self.texture_id)

        # Tell OpenGL how to scale images
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)

        # Tell OpenGL that data is aligned to byte boundries
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1)

    #Llama a las texturas
    def crear(self):

        # Get the dimensions of the image
        width, height = self.texture_surface.get_rect().size

        gluBuild2DMipmaps(GL_TEXTURE_2D,
                          3,
                          width,
                          height,
                          GL_RGB,
                          GL_UNSIGNED_BYTE,
                          self.texture_data)

    def destructor(self):
        text = Textura()
        glDeleteTextures(text.texture_id)
