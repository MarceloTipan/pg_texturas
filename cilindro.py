# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Diciembre 2020
# Clase Cilindro
# Descripción: Una clase que mantiene todos los componentes necesarios para la creación del cilindro con texturas
# Es llamada por la clase Main.

from OpenGL.GLU import *
from Texturas import *

class Cilindro:
    #Inicializador
   def __init__(self, gl):
        self.gl = gl
        self.tx1 = Textura(3)
        self.tx2 = Textura(2)

    #Creación del cilindro
   def crearCilindro(self,base,top,h, sl, st, ex,ey,ez,tx,ty,tz,rx,ry,rz):
       self.gl.glPushMatrix()
        #Transformaciones
       self.gl.glScalef(ex, ey, ez)
       self.gl.glTranslate(tx, ty, tz)
       self.gl.glRotate(rx, 1, 0, 0)
       self.gl.glRotate(ry, 0, 1, 0)
       self.gl.glRotate(rz, 0, 0, 1)

        #Dibujamos el cilindro
       cylinder = gluNewQuadric()
       gluQuadricDrawStyle(cylinder, GLU_FILL);
       gluQuadricTexture(cylinder, True);
       gluQuadricNormals(cylinder, GLU_SMOOTH);
       self.tx1.crear()
       gluCylinder(cylinder,base,top,h, sl, st)

       self.gl.glPopMatrix()

   #Método para crear el cilindro de alambre, Wireframe
   def crearCilindroWireFrame(self, r, g, b, s ,base,top,h, sl, st, ex,ey,ez,tx,ty,tz,rx,ry,rz):
       self.gl.glPushMatrix()
       # Transformaciones
       self.gl.glScalef(ex, ey, ez)
       self.gl.glTranslate(tx, ty, tz)
       self.gl.glRotate(rx, 1, 0, 0)
       self.gl.glRotate(ry, 0, 1, 0)
       self.gl.glRotate(rz, 0, 0, 1)

       self.gl.glColor3f(r, g, b)
       self.gl.glLineWidth(s)#grosor de linea
       cylinder = gluNewQuadric()
       gluQuadricDrawStyle(cylinder, GLU_LINE);#para graficar cilindro tipo wireframe o alambre
       self.gl.glColor3f(r, g, b)
       gluCylinder(cylinder, base, top, h, sl, st)

       self.gl.glPopMatrix()
